package ParkingLotManager;

import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Car;
import ParkingLotManager.Interfaces.EntityInterface;
import ParkingLotManager.Entities.Pedestrian;
import ParkingLotManager.Entities.SupplyCar;
import ParkingLotManager.Entities.Tank;
import ParkingLotManager.Entities.TeacherCar;
import ParkingLotManager.Entities.EmergencyCar;

import java.util.ArrayList;
import java.util.Random;

final public class ParkingLot {
	Random losowe2 = new Random();

    private ArrayList<EntityInterface> entitiesOnProperty = new ArrayList<>();
    private int carsOnProperty = 0;
    private int maxplaces = 500;
    private float money = 0;
    private int BicyclesOnBikeRack = 0;
    private int maxBicycles = 20;

    public boolean checkIfCanEnter(EntityInterface entity) {
        return entity.canEnter();
    }

    public void letIn(EntityInterface entity) {
        entitiesOnProperty.add(entity);
        
        if(entity instanceof Car&&carsOnProperty<maxplaces) {
        	int index = losowe2.nextInt(101);
        	Log.info(entity.identify() + " let in.");
        	if (index>=0&&index<=50) {
        	Log.info("We sold a single Ticket");
        	money = (float) (money+1.50);
        	}
        	if (index>=51&&index<=75) {
            Log.info("We sold 24 hour Ticket");
            money = (float) (money+5.00);
            }
        	if (index>=76&&index<=90) {
            Log.info("We sold 1 month Ticket");
            money = (float) (money+20.00);
            }
        	if (index>=91&&index<=100) {
            Log.info("This vehicle already have a Ticket and don't need a second one!");
            }
            carsOnProperty++;    
    	}
        
        if(entity instanceof TeacherCar&&carsOnProperty<maxplaces) {
        	Log.info(entity.identify() + " let in.");
        	Log.info("This is teacher car he don't pay for entry!");
            carsOnProperty++;    
    	}
        
        if(entity instanceof EmergencyCar) {
        	Log.info(entity.identify() + " let in.");
        	Log.info("This is Emergency car he don't pay for entry!");
        	Log.info("This is an emergency car. He does not use the parking lot!");
        }
        
        if(entity instanceof SupplyCar) {
        	Log.info(entity.identify() + " let in.");
        	Log.info("This is Supply car he don't pay for entry!");
        	Log.info("This is an Supply car. He does not use the parking lot!");
        }
        
    	if(entity instanceof Car&&maxplaces==carsOnProperty) {
    		Log.info(entity.identify() + " Do not Let in becose parking is full!.");
    	}
    	
    	if(entity instanceof Pedestrian) {
    		Log.info(entity.identify() + " Let in.");
    	}
    	
    	if(entity instanceof Bicycle&&BicyclesOnBikeRack<maxBicycles) {
    		Log.info(entity.identify() + " Let in.");
    		BicyclesOnBikeRack++;
    	}
    	
    	if(entity instanceof Bicycle&&BicyclesOnBikeRack==maxBicycles) {
    		Log.info(entity.identify() + " Do not Let in. Bike Rack is full!.");
    	}
    	
    	if(entity instanceof Tank) {
    		Log.info(entity.identify() + " Do not Let in! He will not fit, he is too wide!");
    	}
    	
    }

    public int countCars() {
        return carsOnProperty;
    }
    public float treasury() {
    	return money;
    }

}
