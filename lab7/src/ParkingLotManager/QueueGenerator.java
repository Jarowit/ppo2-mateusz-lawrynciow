package ParkingLotManager;

import ParkingLotManager.Entities.Bicycle;
import ParkingLotManager.Entities.Car;
import ParkingLotManager.Entities.EmergencyCar;
import ParkingLotManager.Entities.Pedestrian;
import ParkingLotManager.Entities.SupplyCar;
import ParkingLotManager.Entities.Tank;
import ParkingLotManager.Entities.TeacherCar;
import ParkingLotManager.Interfaces.EntityInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class QueueGenerator {
	static Random losowe = new Random();

    private static final int ANONYMOUS_PEDESTRIANS_COUNT = losowe.nextInt(101)+1;
    private static final int PEDESTRIANS_COUNT = losowe.nextInt(21)+1;
    private static final int CARS_COUNT = losowe.nextInt(16)+1;
    private static final int TEACHER_CARS_COUNT = losowe.nextInt(16)+1;
    private static final int BICYCLES_COUNT = losowe.nextInt(3)+1;
    private static final int EMERGENCY_CAR_COUNT = losowe.nextInt(2)+1;
    private static final int SUPPLY_CAR_COUNT = losowe.nextInt(5)+1;
    private static final int TANK_COUNT = losowe.nextInt(1)+1;

    public static ArrayList<EntityInterface> generate() {
        ArrayList<EntityInterface> queue = new ArrayList<>();

        for (int i = 0; i <= ANONYMOUS_PEDESTRIANS_COUNT; i++) {
            queue.add(new Pedestrian());
        }

        for (int i = 0; i <= PEDESTRIANS_COUNT; i++) {
            queue.add(new Pedestrian(getRandomName()));
        }

        for (int i = 0; i <= CARS_COUNT; i++) {
            queue.add(new Car(getRandomPlateNumber()));
        }

        for (int i = 0; i <= TEACHER_CARS_COUNT; i++) {
            queue.add(new TeacherCar(getRandomPlateNumber()));
        }

        for (int i = 0; i <= BICYCLES_COUNT; i++) {
            queue.add(new Bicycle());
        }
        
        for (int i = 0; i <= EMERGENCY_CAR_COUNT; i++) {
            queue.add(new EmergencyCar(getRandomPlateNumber()));
        }
        
        for (int i = 0; i <= SUPPLY_CAR_COUNT; i++) {
            queue.add(new SupplyCar(getRandomPlateNumber()));
        }
        
        for (int i = 0; i <= TANK_COUNT; i++) {
            queue.add(new Tank());
        }
        
        

        Collections.shuffle(queue);

        return queue;
    }

    private static String getRandomPlateNumber() {
        Random generator = new Random();
        return "CJH " + (generator.nextInt(89999) + 10000);
    }

    private static String getRandomName() {
        String[] names = {"John", "Jack", "James", "George", "Joe", "Jim"};
        return names[(int) (Math.random() * names.length)];
    }

}
