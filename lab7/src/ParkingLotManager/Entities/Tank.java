package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;

public class Tank implements EntityInterface {

    public String identify() {
        return "This is a Tank!";
    }

    public boolean canEnter() {
        return true;
    }

}