package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;

public class TeacherCar implements EntityInterface {

    private String plate;

    public TeacherCar(String plate) {
        this.plate = plate;
    }

    public String identify() {
        return "Teacher Car with plate number " + plate;
    }

    public boolean canEnter() {
        return true;
    }

}