package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;

public class SupplyCar implements EntityInterface {

    private String plate;

    public SupplyCar(String plate) {
        this.plate = plate;
    }

    public String identify() {
        return "Supply Car with plate number " + plate;
    }

    public boolean canEnter() {
        return true;
    }

}
