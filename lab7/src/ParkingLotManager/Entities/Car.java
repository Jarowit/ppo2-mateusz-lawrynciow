package ParkingLotManager.Entities;

import java.util.Random;

import ParkingLotManager.Log;
import ParkingLotManager.Interfaces.EntityInterface;

public class Car implements EntityInterface {

    private String plate;

    public Car(String plate) {
        this.plate = plate;
    }

    public String identify() {
        return "Car with plate number " + plate;
    }

    public boolean canEnter() {
    	Random losowe3 = new Random();
    	int index2 = losowe3.nextInt(101);
    	if(index2>=0&&index2<=90) {
    		return true;
    	}else{
    		Log.info("This Car " + plate +" is on the black list");
    		return false;
    	}
    }
}
