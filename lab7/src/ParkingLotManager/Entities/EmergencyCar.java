package ParkingLotManager.Entities;

import ParkingLotManager.Interfaces.EntityInterface;

public class EmergencyCar implements EntityInterface {

    private String plate;

    public EmergencyCar(String plate) {
        this.plate = plate;
    }

    public String identify() {
        return "Emergency Car with plate number " + plate;
    }

    public boolean canEnter() {
        return true;
    }

}

